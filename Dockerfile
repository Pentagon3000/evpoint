# syntax=docker/dockerfile:1
FROM python:3.8.5-alpine
ENV PYTHONBUFFERED 1
RUN apk add --no-cache mariadb-connector-c-dev
RUN apk update && apk add python3 python3-dev mariadb-dev build-base && pip3 install mysqlclient && apk del python3-dev mariadb-dev build-base
RUN apk add netcat-openbsd
USER root
RUN chmod 775 $HOME 
RUN mkdir -p $HOME/app/backend
ENV BACKEND_PATH=/home/app/backend
WORKDIR $BACKEND_PATH
COPY ./app/backend/requirements.txt $BACKEND_PATH
COPY ./app/backend $BACKEND_PATH
# copy entrypoint.prod.sh
COPY ./etc/entrypoint_db.sh /etc
RUN sed -i 's/\r$//g'  /etc/entrypoint_db.sh
RUN chmod +x  /etc/entrypoint_db.sh

ENV APP_HOME=/home/app
# copy project
COPY . $APP_HOME


RUN adduser -D evpoint
# chown all the files to the app user
RUN chown -R evpoint:evpoint $APP_HOME
RUN chmod -R 755 $APP_HOME/backend
# change to the app user
USER evpoint

# run entrypoint.prod.sh
ENTRYPOINT ["/etc/entrypoint_db.sh"]
