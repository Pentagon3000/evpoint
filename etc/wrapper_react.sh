#!/bin/bash
nginx -g "daemon off;" &
cd backend/login
yarn
yarn start &
yarn run local-build
cd ../../
cd backend/frontend
yarn
yarn local-build
yarn
yarn start &
chown evpoint backend/
su -s /bin/bash evpoint -c ' cd /home/evpoint/backend && python manage.py runserver 127.0.0.1:8000'
